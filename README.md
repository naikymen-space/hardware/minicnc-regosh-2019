# minicnc

## Intro

Dentro do projeto da Bancada de Hiperobjetos, existe o minicnc.

El objetivo es construir y documentar una máquina CNC, que pueda dibujar sobre un papel. Es, sobre todo, un ejercicio de introducción a los hiperobjetos.

Está basado en Arduino, y usa componentes de unidades de CD/DVD reutilizadas.

## miniCNC 0.1

Este repositorio contiene información sobre el miniCNC 0.1, basado en turoriales disponibles en internet.

Como es un ejemplo educativo, el firmware y el hardware son es muy simples.

Video demostrativo: https://vimeo.com/352326473

El próximo paso del desarrollo sería el miniCNC 0.2, actualizando el firmware y usando hardware un poco más potente.

### Recursos

En el [repositorio](https://gitlab.com/naikymen-space/hardware/minicnc-regosh-2019/) se encuentran los planos y el código, con algunos ejemplos.

Pasar a la [wiki](https://gitlab.com/naikymen-space/hardware/minicnc-regosh-2019-wiki) para encontrar la documentación e instrucciones paso a paso.

Hicimos videos para cada parte del desarrollo y otro documentando el uso del miniCNC.

### Equipo y Contactos

  * Renan Soares (mantainer) renan.soares@ufgrs.br
  * Nicolás Mendez nmendez.ar@gmail.com  
  * Sol Verniers solardatasystem@protonmail.com

