# Relatorio MiniCNC - Res CTA Gosh 2019

## Projecto y Objetivos Mini CNC

### Objetivos

* Transmitir el conocimiento de herramientas de fabricación, en particular de CNC.

* Generar Documentación de fabricación, de uso (hiperpipeline), funcionamiento, materiales de divulgación y comunicación(video).

* Estudiar diferentes herramientas para CNC (láser, lapicera, extrusor, y softwares que se pueden usar).

* Replicar este desarrollo en otros lugares y nodos.

* Generar una página de Hiperobjetos con la información que generemos.


## Relatorio Día 1 - 24/07

* Conformamos el equipo: Renan, Danielle, Santiago, Juan, Nico y Sol.
* Nos reunimos en el sector de fresadoras, impresoras y herramientas del CTA.
* Hablamos de los objetivos que pensaba cada unx para el proyecto y que pensabamos que era realizable. Cómo se puede expandir el proyecto.
* Renan nos habló de la potencialidad del CNC.
* Se creó el git y las primeras tareas: Objetivos, conseguir lectoras de dvd, armar la wiki, hacer un taller de FreeCAD.
* Conseguimos o DVD, desarmamos uno de prueba y guardamos otro para hacer un video documentado.
* Decidimos que ibamos a tomar fotos del video directamente.
* La documentación se hará también por escrito.
* Se hizo la planta de filmación, elegimos color blanco de fondo.
* Se filmó con dos cámaras una cenital y otra de celular, de costado.
* Se armó el cronograma borrador para pensar el trabajo de estas semanas.


## Relatorio Día 2 - 25/07

* Nos dividimos en equipo
* Se revisó la edición de videos
* Se armó el modelado inicial de la estructura en FreeCad, luego de tomar medidas.
* Se avanzó con la documentación de escritura de instructivo de desarme de dvd.
* Se empezó a leer el texto de hiperobjetos para comenzar con bases fuertes en la campaña de comunicación.
* Se armó la estructura de la campaña de comunicación.
* Se ubicaron las polaridades para conectar los motores.
* Se soldaron los dos motores de los sistemas que quitamos de los dvd.
* Se cortó y limó las piezas de maderas para que quedaran listas para el armado de estructura.
* Se hicieron al menos 3 videos diferentes del proceso.


## Relatorio Día 3 - 26/07

* Seguimos con la lectura de HiperObjetos para avanzar con el tema de comunicación
* Se consiguieron más dvd para desarmar
* Avanzamos con la estructura
* Se armó un boceto inicial
* Se inició la investigación del firmware
* Se comenzó el armado del conector de energía del arduino
* Documentamos de forma visual
* Los motores passo a passo foram testados

## Relatorio Día 4 - 29/07

* Hoy el colectivo tomó las clases de Micelio y FreeCad, que habiamos calendarizado.
* Tuvimos una reunión de estatus de las actividades que estamos realizando, que hay pendiente, que se adelantó.
* Repasamos uso de git y consensuamos el uso para que sea mejor el desarrollo.
* Normalizamos ciertos archivos.
* Comentamos los progresos al momento de comunicación, de documentación y firmware.
* Seleccionamos la plantilla de la web
* Se terminó de leer el texto de hiperobjetos de pezzi.

## Relatorio Día 5 - 30/07

* Hoy el colectivo tomó las clases de KiCad casi todo el día.
* Se generó la estructura nueva para el desarrollo.
* Decidimos postergar el repaso de documentación ya que tenemos que aún terminar la estructura.
* Comentamos los progresos al momento de comunicación, de documentación y firmware.
* Se definieron los colores e inicializamos las carpetas de comunicación y diseño.

## Relatorio Día 6 - 31/07

* Hoy el colectivo tomó las capacitaciones de Blender, que nos sirvió mucho.
* Avanzamos con las propuestas de identidad y comunicación.
* Terminamos la estructura.
* Se montaron motores paso a paso.
* Se sumó un compañero al equipo, Marce, para hacer algunos diseños con blender.

## Relatorio Día 7 - 01/08

* Hoy conseguimos acer el MiniCNC funcionar!
* Conseguimos acer alguns desenios repetidas vezes sem problemas.
* Começamos una nova etapa para a eletrônica, para cambiar la shield atual por la shield CNC 3.0 .
* El CNC Grande teve seus motores passo a passo testados e parece funcionar.
* Hoy se redefinió la estetica y las bases de comunicación de REGOSH!!

## Relatorio Día 8 - 02/08

* Hicimos una recorrida por el campus de la universidad con visitas a los institutos.
* Hizo la prueba de poster frontal en a4 para ver dimensiones.
* Avanzamos con pruebas del MiniCNC

## Relatorio Día 9 - 05/08

* Se hizo trabajo de comunicación: posters, diseños para web, diseños para redes sociales de los 9 proyectos. 
* Se armó un poster general de REGOSH
* Se trabajo en videos y documentación
* Se probo diferentes software para preparar el gcode

## Relatorio Día 10 - 06/08

* Se terminaron las piezas comunicacionales generales.
* Se realizó las pruebas en una imprenta Toka da Copia  http://www.tokadacopia.com.br/
* Se normalizaron los archivos de diseño para que se puedan usar correctamente
* Se avanzó con las visuales de blender
* Se hicieron pruebas en el CNC con el log de regosh
