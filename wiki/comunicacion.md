# Comunicación Estrategica para Tecnologías Libres

La comunicación como territorio a conquistar ante el avance de los medios hegemónicos es clave para difundir, comunicar, alfabetizar tecnológica y digitalmente, basandonos en la cultura libre, la ciencia abierta y compartir los conocimientos con la comunidad.
En ese sentido establecemos una estrategia para comunicar la Bancada de los Hiperobjetos, en particular el desarrollo del mini CNC realizado en la residencia de CTA, en Porto Alegre, 2019.

Por eso establecemos las siguientes piezas, tonos y necesidades


## Mensaje que queremos transmitir:
- La importancia de las tecnologías libres en todos los planos.
- El concepto de HiperObjeto como instancia necesaria para desarrollo de tecnologías libres, en particular el hardware.
- El trabajo y autoformación colaborativa.
- Conocimientos libres técnicos relativos al desarrollo.
- Ideas de expansión y uso.

### Para pensar la comunicación en el tono y lenguaje establecemos:
- Inclusividad en géneros
- Que sea en español y portugués de ser posible.

### Para esto realizaremos las siguientes piezas comunicacionales.

- Instructivo diseñado con el paso a paso orientado a personas facilitadoras o docentes.
- Presentación de diapositivas para ayudar en la transmisión del proceso de Mini CNC.
- Piezas comunicacionales para Redes sociales de uso masivo contemplando diseños 1200 x 1200, con texto redactado para las diversas necesidades. Las piezas tendrán 3 ejes: tecnologías libres, Hiperobjetos, MiniCNC.
- Un sitio web que explique el proyecto, que tenga el video incrustado, linkee al repo, que tenga el archivo de descarga de tutorial, de presentación y links a los elementos que lo construyen, siguiendo la linea de HiperObjeto. 
- Para la presentación de la residencia, pensaremos piezas comunicacionales físicas (posters, dibujos hechos por el cnc, otros) que puedan ser expuestos junto al desarrollo.


