# MiniCNC Wiki

## Descripción
Fabricación de un hiperobjeto como ejercicio para comprender el paradigma general.

El ejercicio consiste en diseñar un "MiniCNC" reciclando lectoras de CD/DVD, para apropiarse de la tecnología y los métodos libres de desarrollo, documentación y fabricación.

### Objetivos

  * Transmitir el conocimiento de herramientas de fabricación, en particular de CNC.
  * Generar Documentación de fabricación, de uso (hiperpipeline), funcionamiento, materiales de divulgación y comunicación(video).
  * Estudiar diferentes herramientas para CNC (láser, lapicera, extrusor, y softwares que se pueden usar).
  * Replicar este desarrollo en otros lugares y nodos.
  * Generar una página de Hiperobjetos con la información que generemos.

## Equipo y Contactos
  * Renan Soares (mantainer) renan.soares@ufgrs.br
  * Nicolás Mendez sir.ssh@gmail.com  
  * Sol Verniers solardatasystem@protonmail.com
  * Santiago
  * Juan
  * Danielli


## Teoría de Funcionamiento

Explicación detallada del funcionamiento de una máquina CNC [aquí](teoria).

## Para que puede servir un CNC

Fabricação  digital ou  fabricação  personalizada  consiste  na  materialização de objetos a partir de desenhos e representações digitais utilizando-se métodos aditivos ou subtrativos de materiais controlados numericamente por computador (CNC), a fim de se obter um  objeto  físico  com  as  características  desejadas.  Pode-se  citar  os  seguintes métodos de fabricação digital:

  * Fabricação aditiva (impressão 3D)
  * Impressão de termoplásticos (polímeros)
  * Impressão de metais
  * Impressão a partir de pó (cerâmicas e metais)
  * Fabricação subtrativa
  * Fresadoras e tornos CNC
  * Usinagem por descarga elétrica (Electrical Discharge Machi-
ning – EDM)
  * Cortadoras a laser e a plasma
  * Centros de usinagem

### Hiperobjetos


El concepto de hiperobjetos lo acuño Rafael Pezzi y es el concepto fundamental de la Bancada de los HiperObjetos, herramientas y máquinas que permiten desarrollar con Open Hardware otras herramientas y desarrollos de investigación.

Básicamente busca promover que las herramientas sean libres tanto de diseño, como estudio y fabricación, tomando de referencia los Hiperlinks como medios de relacionamiento y llevandolo al concepto de HiperObjeto.
Un objeto es HiperObjeto cuando comparte la información para ser creado, para compartir, modificar y materializarlo. 
Los objetos a diferencia del conocimiento sólo tienen una materialidad con un uso limitado y por eso es importante promover sus usos simultaneos promoviendo la difusión del conocimiento de la creación de esos objetos. 

### MiniCNC
Usando motores paso a paso, es posible traducir instrucciones digitales (Gcode) a desplazamientos en el espacio físico (milímetros).

Estos desplazamientos conforman la trayectoria de una lapicera (o cualquier herramienta), de manera tal que la máquina puede hacer un dibujo arbitrario en una superficie como el papel.

El dibujo es generado previamente usando un programa de diseño asistido por computadora (FreeCAD), que luego es traducido a Gcode por software CAM.

Finalmente, la electronica del MiniCNC interpreta y ejecuta las instrucciones del Gcode, moviendo los motores paso a paso.

Estos principios de funcionamiento son equivalentes a los de cualquier máquina de CNC (fresadoras, cortadoras láser, impresoras 3D, brazos robóticos, etc.)

## Instrucciones de Fabricación

Ver detalles en la página de [instrucciones](instrucciones).

### Materiales
  * Dos lectoras de CD o DVD.
  * Arduino UNO
  * Arduino CNC Shield
  * Fuente de 12V
  * Computadora
  * Soldador
  * Estaño
  * Cables
  * Estructura (puede ser hecha con madera, materiales reciclables u otros)

### Herramientas y Software
  * FreeCAD
  * [fritzing](fritzing.org)
  * ...

### Habilidades Necesarias

## Sugerencias de desarrollo futuro
