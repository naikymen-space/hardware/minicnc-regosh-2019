## Recursos

* Cámara: smarphone + DSLR.

* Software: SimpleScreenRecorder + Screenkey para grabar la pantalla de la computadora; y KDEnlive para editar los videos.

## reGOSH - CTA - MiniCNC - 01 Desarmar DVD

https://vimeo.com/350152546

Como desarmar el DVD para el miniCNC v0.1

https://tecnologias.libres.cc/hiperobjetos/minicnc/
https://tecnologias.libres.cc/hiperobjetos/minicnc/wikis/home 

## reGOSH - CTA - MiniCNC - 02 Preparar Cables

https://vimeo.com/352120131

Como preparar cables IDE para el miniCNC v0.1

https://tecnologias.libres.cc/hiperobjetos/minicnc/
https://tecnologias.libres.cc/hiperobjetos/minicnc/wikis/home 

## reGOSH - CTA - MiniCNC - 03 Soldar Cables a Motores

https://vimeo.com/352123854

Como soldar cables IDE a los motores del miniCNC v0.1

https://tecnologias.libres.cc/hiperobjetos/minicnc/
https://tecnologias.libres.cc/hiperobjetos/minicnc/wikis/home 

## reGOSH - CTA - MiniCNC - 04 Modo de Uso Completo

https://vimeo.com/352326473

Como usar el miniCNC v0.1

https://tecnologias.libres.cc/hiperobjetos/minicnc/
https://tecnologias.libres.cc/hiperobjetos/minicnc/wikis/home

## reGOSH - CTA - MiniCNC - 05 Montaje de la estructura

Esta fue una de las etapas más largas [https://www.youtube.com/watch?v=zRo8iZI_7W0](https://www.youtube.com/watch?v=zRo8iZI_7W0).


