# Cómo desarmar el DVD
Instrucciones para extraer los motores paso a paso de las lectoras de CD o DVD.

:)

Hay un [video](https://vimeo.com/350152546) disponible (editado en Kdenlive).

## Materiales
  * Destornilladores
  * Pinzas
  * Unidad de DVD
  * Alambre o aguja

## 01 - Remover la carcasa

  1. Extender la bandeja hacia afuera usando el alambre.
  2. Quitar el tope de la bandeja.
  3. Retraer la bandeja nuevamente.
  4. Quitar los tornillos de la carcasa.
  5. Quitar la tapa de la carcasa.
  6. Quitar el frente de plástico (para destrabarla, ayuda apretar las trabitas de plástico a los costados).
  7. 

## 02 - Remover componentes no utilizados (1/2)

  1. Desconectar los cables o cintas, y remover las placas controladoras.
  2. Se puede quitar la base de la carcasa.
  3. Para poder obtener la estructura con los motores (carrito), hay que destrabar la bandeja y extenderla hacia afuera nuevamente.
  4. Luego se puede desatornillar el carrito y quitarlo.

## 03 - Remover componentes no utilizados (2/2)

  1. Desatornillar y remover el motor del CD.
  2. Usar destornillador y pinzas para quitar todos los componentes 
  ópticos.

![Inline image](1-desarmarDVD_DSC_2244.jpg)

![Inline image](1-desarmarDVD_DSC_2254.jpg)

