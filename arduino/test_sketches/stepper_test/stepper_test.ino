// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>

// Connect a stepper motor with 48 steps per revolution (7.5 degree)
// to motor port #2 (M3 and M4)
AF_Stepper motor1(48, 1);
AF_Stepper motor2(48, 2);

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Stepper test!");

  motor1.setSpeed(10);  // 10 rpm   
  motor2.setSpeed(10);  // 10 rpm   
}

void loop() {
  Serial.println("Single coil steps");
//  motor1.step(100, FORWARD, SINGLE); 
//  motor1.step(100, BACKWARD, SINGLE); 

//  Serial.println("Double coil steps");
//  motor2.step(100, FORWARD, DOUBLE); 
//  motor2.step(100, BACKWARD, DOUBLE);

//  Serial.println("Interleave coil steps");
//  motor1.step(100, FORWARD, INTERLEAVE); 
//  motor1.step(100, BACKWARD, INTERLEAVE); 

//  Serial.println("Micrsostep steps");
  motor1.step(10, FORWARD, MICROSTEP); 
  motor1.step(10, BACKWARD, MICROSTEP); 
}
