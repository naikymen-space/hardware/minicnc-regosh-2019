
G21 (metric ftw)
G90 (absolute mode)
G92 X0.00 Y0.00 Z0.00 (you are here)

M300 S50 (pen up)
G4 P50 (wait 50ms)


M300 S30 (pen down)
G4 P50 (wait 50ms)

G1 X10 Y0
G4 P1000 (wait 1000ms)


G1 X10 Y10
G4 P1000 (wait 1000ms)

G1 X0 Y10
G4 P1000 (wait 1000ms)

G1 X0 Y0
G4 P1000 (wait 1000ms)


(end of print job)
M300 S50.00 (pen up)
G4 P50 (wait 50ms)
M300 S255 (turn off servo)
G1 X0 Y0 F3000.00
G1 Z0.00 F150.00 (go up to finished level)
G1 X0.00 Y0.00 F3000.00 (go home)
M18 (drives off)
