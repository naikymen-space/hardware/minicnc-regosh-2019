G40	; disable tool radius compensation
G49	; disable tool length compensation
G80	; cancel modal motion
G54	; select coordinate system 1
G90	; disable incremental moves
G21	; metric
F300	; set feedrate
S1000	; set spindle speed
G61	; exact path mode
M3	; start spindle
G04 P3.0	; wait for 3.0 seconds
G0 X0.000000 Y25.604770 Z25.000000
F100	; set feedrate
G1 Z0.000000
F300	; set feedrate
G1 Y12.122490
  X37.727287
  Y25.604770
  X0.000000
G0 Z25.000000
  X5.105310 Y31.563490
F100	; set feedrate
G1 Z0.000000
F300	; set feedrate
G1 Y6.163810
  X32.621976
  Y31.563490
  X5.105310
G0 Z25.000000
  X12.122505 Y37.727290
F100	; set feedrate
G1 Z0.000000
F300	; set feedrate
G1 Y0.000000
  X25.604781
  Y37.727290
  X12.122505
M5	; stop spindle
G0 Z25.000000
M2	; end program
