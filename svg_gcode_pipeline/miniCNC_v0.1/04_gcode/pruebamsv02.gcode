G40	; disable tool radius compensation
G49	; disable tool length compensation
G80	; cancel modal motion
G54	; select coordinate system 1
G90	; disable incremental moves
G21	; metric
F300	; set feedrate
S1000	; set spindle speed
G61	; exact path mode
M3	; start spindle
G04 P3.0	; wait for 3.0 seconds
G0 X22.000000 Y7.489663 Z25.000000
F100	; set feedrate
G1 Z0.000000
F300	; set feedrate
G1 X36.510337 Y22.000000
  Y22.000000
  X22.000000 Y36.510337
  X22.000000
  X7.489663 Y22.000000
  X7.489663 Y22.000000
  X22.000000 Y7.489663
G0 Z25.000000
  X22.000000 Y12.326442
F100	; set feedrate
G1 Z0.000000
F300	; set feedrate
G1 X31.673558 Y22.000000
  Y22.000000
  X22.000000 Y31.673558
  X22.000000
  X12.326442 Y22.000000
  Y22.000000
  X22.000000 Y12.326442
G0 Z25.000000
  X22.000000 Y17.163221
F100	; set feedrate
G1 Z0.000000
F300	; set feedrate
G1 X26.836779 Y22.000000
  Y22.000000
  X22.000000 Y26.836779
  X22.000000
  X17.163221 Y22.000000
  Y22.000000
  X22.000000 Y17.163221
M5	; stop spindle
G0 Z25.000000
M2	; end program
